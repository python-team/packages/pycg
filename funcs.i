
typedef void (*CGerrorCallbackFunc)(void);

/*** Context functions ***/

CGcontext cgCreateContext(void); 
void cgDestroyContext(CGcontext ctx); 
CGbool cgIsContext(CGcontext ctx);
const char *cgGetLastListing(CGcontext ctx);
void cgSetAutoCompile(CGcontext ctx, CGenum flag);

/*** Program functions ***/
CGprogram cgCreateProgram(CGcontext ctx, 
		                    CGenum program_type,
				    const char *program,
				    CGprofile profile,
				    const char *entry,
				    const char **args);
CGprogram cgCreateProgramFromFile(CGcontext ctx, 
                                            CGenum program_type,
                                            const char *program_file,
                                            CGprofile profile,
                                            const char *entry,
                                            const char **args);
CGprogram cgCopyProgram(CGprogram program); 
void cgDestroyProgram(CGprogram program); 

CGprogram cgGetFirstProgram(CGcontext ctx);
CGprogram cgGetNextProgram(CGprogram current);
CGcontext cgGetProgramContext(CGprogram prog);
CGbool cgIsProgram(CGprogram program); 

void cgCompileProgram(CGprogram program); 
CGbool cgIsProgramCompiled(CGprogram program); 
const char *cgGetProgramString(CGprogram prog, CGenum pname); 
CGprofile cgGetProgramProfile(CGprogram prog); 

/*** Parameter functions ***/

CGparameter cgCreateParameter(CGcontext ctx, CGtype type);
CGparameter cgCreateParameterArray(CGcontext ctx,
                                             CGtype type, 
                                             int length);
CGparameter cgCreateParameterMultiDimArray(CGcontext ctx,
                                                     CGtype type,
                                                     int dim, 
                                                     const int *lengths);
void cgDestroyParameter(CGparameter param);
void cgConnectParameter(CGparameter from, CGparameter to);
void cgDisconnectParameter(CGparameter param);
CGparameter cgGetConnectedParameter(CGparameter param);

int cgGetNumConnectedToParameters(CGparameter param);
CGparameter cgGetConnectedToParameter(CGparameter param, int index);

CGparameter cgGetNamedParameter(CGprogram prog, const char *name);
CGparameter cgGetNamedProgramParameter(CGprogram prog, 
                                                 CGenum name_space, 
                                                 const char *name);

CGparameter cgGetFirstParameter(CGprogram prog, CGenum name_space);
CGparameter cgGetNextParameter(CGparameter current);
/*CGparameter cgGetFirstLeafParameter(CGprogram prog, CGenum name_space);
CGparameter cgGetNextLeafParameter(CGparameter current);*/

CGparameter cgGetFirstStructParameter(CGparameter param);
CGparameter cgGetNamedStructParameter(CGparameter param, 
                                                const char *name);

CGparameter cgGetFirstDependentParameter(CGparameter param);

CGparameter cgGetArrayParameter(CGparameter aparam, int index);
int cgGetArrayDimension(CGparameter param);
CGtype cgGetArrayType(CGparameter param);
int cgGetArraySize(CGparameter param, int dimension);
void cgSetArraySize(CGparameter param, int size);
void cgSetMultiDimArraySize(CGparameter param, const int *sizes);

CGprogram cgGetParameterProgram(CGparameter param);
CGcontext cgGetParameterContext(CGparameter param);
CGbool cgIsParameter(CGparameter param);
const char *cgGetParameterName(CGparameter param);
CGtype cgGetParameterType(CGparameter param);
CGtype cgGetParameterNamedType(CGparameter param);
const char *cgGetParameterSemantic(CGparameter param);
CGresource cgGetParameterResource(CGparameter param);
CGresource cgGetParameterBaseResource(CGparameter param);
unsigned long cgGetParameterResourceIndex(CGparameter param);
CGenum cgGetParameterVariability(CGparameter param);
CGenum cgGetParameterDirection(CGparameter param);
CGbool cgIsParameterReferenced(CGparameter param);
const double *cgGetParameterValues(CGparameter param, 
                                             CGenum value_type,
                                             int *nvalues);
int cgGetParameterOrdinalNumber(CGparameter param);
CGbool cgIsParameterGlobal(CGparameter param);
int cgGetParameterIndex(CGparameter param);

void cgSetParameterVariability(CGparameter param, CGenum vary);
void cgSetParameterSemantic(CGparameter param, const char *semantic);


void cgSetParameter1f(CGparameter param, float x);
void cgSetParameter2f(CGparameter param, float x, float y);
void cgSetParameter3f(CGparameter param, float x, float y, float z);
void cgSetParameter4f(CGparameter param, 
                                float x, 
                                float y, 
                                float z,
                                float w);
void cgSetParameter1d(CGparameter param, double x);
void cgSetParameter2d(CGparameter param, double x, double y);
void cgSetParameter3d(CGparameter param, 
                                double x, 
                                double y, 
                                double z);
void cgSetParameter4d(CGparameter param, 
                                double x, 
                                double y, 
                                double z,
                                double w);


void cgSetParameter1fv(CGparameter param, const float *v);
void cgSetParameter2fv(CGparameter param, const float *v);
void cgSetParameter3fv(CGparameter param, const float *v);
void cgSetParameter4fv(CGparameter param, const float *v);
void cgSetParameter1dv(CGparameter param, const double *v);
void cgSetParameter2dv(CGparameter param, const double *v);
void cgSetParameter3dv(CGparameter param, const double *v);
void cgSetParameter4dv(CGparameter param, const double *v);

void cgSetMatrixParameterdr(CGparameter param, const double *matrix);
void cgSetMatrixParameterfr(CGparameter param, const float *matrix);
void cgSetMatrixParameterdc(CGparameter param, const double *matrix);
void cgSetMatrixParameterfc(CGparameter param, const float *matrix);


/*** Type Functions ***/

const char *cgGetTypeString(CGtype type);
CGtype cgGetType(const char *type_string);

CGtype cgGetNamedUserType(CGprogram program, const char *name);

int cgGetNumUserTypes(CGprogram program);
CGtype cgGetUserType(CGprogram program, int index);

int cgGetNumParentTypes(CGtype type);
CGtype cgGetParentType(CGtype type, int index);

CGbool cgIsParentType(CGtype parent, CGtype child);
CGbool cgIsInterfaceType(CGtype type);

/*** Resource Functions ***/

const char *cgGetResourceString(CGresource resource);
CGresource cgGetResource(const char *resource_string);

/*** Enum Functions ***/

const char *cgGetEnumString(CGenum en);
CGenum cgGetEnum(const char *enum_string);

/*** Profile Functions ***/

const char *cgGetProfileString(CGprofile profile);
CGprofile cgGetProfile(const char *profile_string);

/*** Error Functions ***/

CGerror cgGetError(void);
const char *cgGetErrorString(CGerror error);
const char *cgGetLastErrorString(CGerror *error);
void cgSetErrorCallback(CGerrorCallbackFunc func);
/*CGerrorCallbackFunc cgGetErrorCallback(void);*/

/*** Misc Functions ***/

const char *cgGetString(CGenum sname);


/*** Support for deprecated Cg 1.1 API ***/
/* CGparameter cgGetNextParameter_depr1_1(CGparameter current); */
/* CGparameter cgGetNextLeafParameter_depr1_1(CGparameter current); */


/* The following are from cgGL.h */

CGbool cgGLIsProfileSupported(CGprofile profile);

void cgGLEnableProfile(CGprofile profile);
void cgGLDisableProfile(CGprofile profile);

CGprofile cgGLGetLatestProfile(CGGLenum profile_type);
void cgGLSetOptimalOptions(CGprofile profile);

/******************************************************************************
 *** Program Managment Functions                                 
 *****************************************************************************/

void cgGLLoadProgram(CGprogram program);
CGbool cgGLIsProgramLoaded(CGprogram program);
void cgGLBindProgram(CGprogram program);
void cgGLUnbindProgram(CGprofile profile);
GLuint cgGLGetProgramID(CGprogram program);

/******************************************************************************
 *** Parameter Managment Functions                                 
 *****************************************************************************/

void cgGLSetParameter1f(CGparameter param,
                                    float x);

void cgGLSetParameter2f(CGparameter param,
                                    float x,
                                    float y);

void cgGLSetParameter3f(CGparameter param,
                                    float x,
                                    float y,
                                    float z);

void cgGLSetParameter4f(CGparameter param,
                                    float x,
                                    float y,
                                    float z,
                                    float w);

void cgGLSetParameter1fv(CGparameter param, const float *v);

void cgGLSetParameter2fv(CGparameter param, const float *v);

void cgGLSetParameter3fv(CGparameter param, const float *v);

void cgGLSetParameter4fv(CGparameter param, const float *v);

void cgGLSetParameter1d(CGparameter param,
                                    double x);

void cgGLSetParameter2d(CGparameter param,
                                    double x,
                                    double y);

void cgGLSetParameter3d(CGparameter param,
                                    double x,
                                    double y,
                                    double z);

void cgGLSetParameter4d(CGparameter param,
                                    double x,
                                    double y,
                                    double z,
                                    double w);

void cgGLSetParameter1dv(CGparameter param, const double *v);

void cgGLSetParameter2dv(CGparameter param, const double *v);

void cgGLSetParameter3dv(CGparameter param, const double *v);

void cgGLSetParameter4dv(CGparameter param, const double *v);

void cgGLGetParameter1f(CGparameter param, float *v);

void cgGLGetParameter2f(CGparameter param, float *v);

void cgGLGetParameter3f(CGparameter param, float *v);

void cgGLGetParameter4f(CGparameter param, float *v);

void cgGLGetParameter1d(CGparameter param, double *v);

void cgGLGetParameter2d(CGparameter param, double *v);

void cgGLGetParameter3d(CGparameter param, double *v);

void cgGLGetParameter4d(CGparameter param, double *v);

void cgGLSetParameterArray1f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const float *v);

void cgGLSetParameterArray2f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const float *v);

void cgGLSetParameterArray3f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const float *v);

void cgGLSetParameterArray4f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const float *v);

void cgGLSetParameterArray1d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const double *v);

void cgGLSetParameterArray2d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const double *v);

void cgGLSetParameterArray3d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const double *v);

void cgGLSetParameterArray4d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         const double *v);

void cgGLGetParameterArray1f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         float *v);

void cgGLGetParameterArray2f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         float *v);

void cgGLGetParameterArray3f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         float *v);

void cgGLGetParameterArray4f(CGparameter param,
                                         long offset,
                                         long nelements,
                                         float *v);

void cgGLGetParameterArray1d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         double *v);

void cgGLGetParameterArray2d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         double *v);

void cgGLGetParameterArray3d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         double *v);

void cgGLGetParameterArray4d(CGparameter param,
                                         long offset,
                                         long nelements,
                                         double *v);

void cgGLSetParameterPointer(CGparameter param,
                                         GLint fsize,
                                         GLenum type,
                                         GLsizei stride,
                                         const GLvoid *pointer);

void cgGLEnableClientState(CGparameter param);
void cgGLDisableClientState(CGparameter param);

/******************************************************************************
 *** Matrix Parameter Managment Functions                                 
 *****************************************************************************/

void cgGLSetMatrixParameterdr(CGparameter param, 
                                          const double *matrix);
void cgGLSetMatrixParameterfr(CGparameter param, 
                                          const float *matrix);
void cgGLSetMatrixParameterdc(CGparameter param, 
                                          const double *matrix);
void cgGLSetMatrixParameterfc(CGparameter param, 
                                          const float *matrix);

void cgGLGetMatrixParameterdr(CGparameter param, double *matrix);
void cgGLGetMatrixParameterfr(CGparameter param, float *matrix);
void cgGLGetMatrixParameterdc(CGparameter param, double *matrix);
void cgGLGetMatrixParameterfc(CGparameter param, float *matrix);

void cgGLSetStateMatrixParameter(CGparameter param, 
                                             CGGLenum matrix,
                                             CGGLenum transform);

void cgGLSetMatrixParameterArrayfc(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               const float *matrices);

void cgGLSetMatrixParameterArrayfr(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               const float *matrices);

void cgGLSetMatrixParameterArraydc(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               const double *matrices);

void cgGLSetMatrixParameterArraydr(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               const double *matrices);

void cgGLGetMatrixParameterArrayfc(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               float *matrices);

void cgGLGetMatrixParameterArrayfr(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               float *matrices);

void cgGLGetMatrixParameterArraydc(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               double *matrices);

void cgGLGetMatrixParameterArraydr(CGparameter param, 
                                               long offset,
                                               long nelements,
                                               double *matrices);

/******************************************************************************
 *** Texture Parameter Managment Functions
 *****************************************************************************/

void cgGLSetTextureParameter(CGparameter param, GLuint texobj);
GLuint cgGLGetTextureParameter(CGparameter param);
void cgGLEnableTextureParameter(CGparameter param);
void cgGLDisableTextureParameter(CGparameter param);
GLenum cgGLGetTextureEnum(CGparameter param);
void cgGLSetManageTextureParameters(CGcontext ctx, CGbool flag);
CGbool cgGLGetManageTextureParameters(CGcontext ctx);

