
PyCg
----
This is a simple wrapper from the NVIDIA Cg package to Python 2.2. It
was originally written by Charles Mason <cmason@cs.fsu.edu> and it has
since been modified by Calle Lejdfors <calle.lejdfors@cs.lth.se>. It
is currently maintained by Calle Lejdfors.


Copyright (C) 2003-2004 Charles Mason
Copyright (C) 2004-2007 Carl Johan Lejdfors


INSTALLATION
------------
This is a bare minimum installation of PyCg. To install it run 

$ python setup.py install

This may require swig (it has been tested 1.3.19) and will most likely 
only work under Linux. To build it under Windows please use the
supplied Cygwin makefile.


USAGE
-----

Check the original Mason PyCg-demos package for some example code.
These demos are basically the same as the nvidia demos that are
available for download. 


CONTACT
-------

If you have any questions, feel free to contact me Calle Lejdfors
<calle.lejdfors@cs.lth.se>. The original author (Charles Mason) is
available at cmason@cs.fsu.edu.

The original PyCg's website is at 

   http://www.csit.fsu.edu/~mason/?section=projects:personal:pycg 

LICENSE
-------

Although a little confused with licensing for the software, I did
re-write most of it, yet a good portion of the media comes from
NVIDIA.  So as per nvidia's request, this statement must be included:

 * THE NVIDIA SOFTWARE IS BEING PROVIDED ON AN "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
 * WITHOUT LIMITATION, WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT,
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR ITS USE AND OPERATION
 * EITHER ALONE OR IN COMBINATION WITH OTHER PRODUCTS.
 *
 *
 *
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * EXEMPLARY, CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOST
 * PROFITS; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) OR ARISING IN ANY WAY OUT OF THE USE,
 * REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE NVIDIA SOFTWARE,
 * HOWEVER CAUSED AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING
 * NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF NVIDIA HAS BEEN ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.

From that, this software is licensed under the GPL version 2.  See
LICENSE for more information. In addition, as a special exception, the
copyright holders of PyCg allows linking to NVIDIA's Cg. 


