#define CG_FALSE ((CGbool)0)
#define CG_TRUE ((CGbool)1)

/* The Callback type -- does SWIG even need this? 
typedef void (*CGerrorCallbackFunc)(void);
/* */

/* Structure Types that are often used */
typedef struct _CGcontext *CGcontext;
typedef struct _CGprogram *CGprogram;
typedef struct _CGparameter *CGparameter;
typedef int CGbool;

typedef unsigned long GLuint;
typedef int GLint;

