%module Cg
%{
#include "Cg/cg.h"
#include "Cg/cgGL.h"
#include <Numeric/arrayobject.h>
%}

%init %{
  import_array();
%}

%typemap(in) int {
  $1 = (int) PyLong_AsLong($input);
}

%typemap(in) float {
  $1 = (float) PyFloat_AsDouble($input);
}

%typemap(in) double {
  $1 = PyFloat_AsDouble($input);
}

%typemap(in) const char *name {
  if (!PyString_Check($input) ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: expected string!" );
    return NULL;
  }

  $1 = PyString_AsString($input);
}

%typemap(in) const char **args (char** tmp) {
  
  tmp = NULL;

  if ( PySequence_Check($input) ) {
    int i;
    int size = PySequence_Size($input);
    tmp = calloc(size, sizeof(char*));

    for ( i = 0 ; i < size; i++ ) {
      PyObject* o = PySequence_GetItem($input, i);
      if ( !PyString_Check(o) ) {
        PyErr_SetString(PyExc_TypeError, "Member of sequence is not a string!");
        goto fail;
      }
      tmp[i] = PyString_AsString(o);
    }

    $1 = tmp;
    
  } else if ( $input == Py_None ) {
    $1 = NULL;

  } else {
    PyErr_SetString(PyExc_TypeError, "Cannot convert argument to const char**");
    goto fail;
  }
    
}

%typemap(freearg) const char **args {
  // $1 = NULL;
  if ( $1 != NULL )
    free($1);
}



%typemap(in) const float * {
  //  PyArrayObject* array;
  
  /*  if ( !PyArray_Check($input) ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: not array!" );
    return NULL;
    }*/
  
  PyArrayObject* array = (PyArrayObject*)PyArray_ContiguousFromObject( $input, PyArray_FLOAT, 1, 1 );

  if ( !array ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: could not cast to float array!" );
    return NULL;
  }

  if ( array->descr->type_num != PyArray_FLOAT ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: not float array!" );
    return NULL;
  }

  $1 = (float*)array->data;
}

%typemap(in) const double * {
/*  if ( !PyArray_Check($input) ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: not array!" );
    return NULL;
  }
  */
  PyArrayObject* array = (PyArrayObject*)PyArray_ContiguousFromObject( $input, PyArray_DOUBLE, 1, 2 );
  
  if ( !array ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: could not cast to float array!" );
    return NULL;
  }

  if ( array->descr->type_num != PyArray_DOUBLE ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: not double array!" );
    return NULL;
  }
  
  $1 = (double*)array->data;
}

%typemap(in) GLenum type {
  $1 = (int) PyInt_AsLong($input);
  if ( PyErr_Occurred() ) {
    return NULL;
  }
}

%typemap(in) GLsizei stride {
  $1 = (int) PyInt_AsLong($input);
  if ( PyErr_Occurred() ) {
    return NULL;
  }
}

%typemap(in) GLvoid *pointer {
  PyArrayObject* array;
  
  if ( !PyArray_Check($input) ) {
    PyErr_SetString(PyExc_ValueError, "CALLE: not array!" );
    return NULL;
  }
  
  if ( array = (PyArrayObject*)PyArray_ContiguousFromObject( $input, PyArray_DOUBLE, 1, 2 ) ) {
    $1 = (double*)array->data;
  } else if ( array = (PyArrayObject*)PyArray_ContiguousFromObject( $input, PyArray_FLOAT, 1, 2 ) ) {
    $1 = (float*)array->data;
  } else {
    PyErr_SetString(PyExc_ValueError, "CALLE: could not cast!" );
    return NULL;
  }
}


%include "types.i"
%include "constants.i"
%include "funcs.i"


/* Error handling function */
%{
/* From PyOpenGL 
 * 
 * In the GLUT section, PyOpenGL uses a similar method to
 * handle callback functions.  I reviewed their source,
 * made minor changes, and did this my way. -CM
 */
#define PyErr_XPrint() if (PyErr_Occurred()) PyErr_Print()

static PyObject *errorCallback = NULL;

void _internalErrorCallback() 
{
	if (errorCallback && errorCallback != Py_None) {
		PyObject *result = PyObject_CallFunction(errorCallback, NULL);
		Py_XDECREF(result);
		PyErr_XPrint();
	}
}

void _cgSetErrorCallback(PyObject *func)
{
	PyObject *old = errorCallback;

	errorCallback = func;

	Py_XINCREF(func);
	cgSetErrorCallback((func == Py_None) ? NULL : _internalErrorCallback);
	Py_XDECREF(old);
}

%}
%name(cgSetErrorCallback) void _cgSetErrorCallback(PyObject * pyfunc);

%{
PyObject *_cgGetErrorCallback()
{
	return errorCallback;
}
%}
%name(cgGetErrorCallback) PyObject *_cgGetErrorCallback();

