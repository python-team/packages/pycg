
from distutils.core import setup,Extension
import os

if os.name == 'nt': 
   data_files=[('Lib/site-packages/Cg', ['_Cg.dll'])]
else:
   data_files=[('lib/python2.5/site-packages/Cg', ['_Cg.so'])]

setup(name='PyCg',
      version='0.14.1',
      description='NVIDIA\'s Cg 1.4 for Python',
      author='Calle Lejdfors',
      author_email='calle.lejdfors@cs.lth.se',
      url='http://graphics.cs.lth.se/pyxf',
      packages=['Cg'],
      ext_modules=[],
      data_files=data_files)

