
include $(shell uname -s).conf

all: _Cg.$(SOEXT)

cg_wrap.c: cg.i *.i
	$(SWIG) -python $(INCLUDES) $<

_Cg.$(SOEXT): cg_wrap.c
	$(LINK) -o $@ $^ $(INCLUDES) $(LIBS)


install: all
	mkdir Cg
	cp Cg.py Cg/__init__.py
	python setup.py $@
	rm -rf Cg

clean:
	rm -f _Cg.$(SOEXT) Cg.pyc Cg.py cg_wrap.c
	rm -rf build dist

.PHONY: clean dist

